From: Teemu Ikonen <tpikonen@gmail.com>
Subject: Fix segfault in image export with GTK.
X-git-branch: p/gtkexport

    File->Export segfaulted when the exported image size was different
    from the image buffer size currently on the screen.

    The fix is to not try to render to user supplied buffer in
    render_buffer(), but to copy FBuffer to the supplied buffer after
    resizing.

    Also fixed the temporary image buffer allocation in export_cb() to use
    sizeof(Pixel) instead of 4.

    Also, replace int with uintptr_t when casting pointers to ints.
